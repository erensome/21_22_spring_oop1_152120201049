#include "ExternalTank.h"

ExternalTank::ExternalTank(double capacity) : Tank(capacity)
{
}

void ExternalTank::Update(string message_)
{
	cout << "Tank " << getTankID() << ": " << message_ << endl;
}

ExternalTank ExternalTank::operator+(ExternalTank& eTank)
{
	double quantityF = this->getFuelQuantity();
	double quantityS = eTank.getFuelQuantity();

	ExternalTank newTank(quantityF + quantityS);
	return newTank;
}
