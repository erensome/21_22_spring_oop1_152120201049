#ifndef SIMULATION_DATA_H
#define SIMULATION_DATA_H

#include <vector>
#include<iterator>
#include<algorithm>
#include<string>
#include"Observer.h"
#include"Subject.h"

class SimulationData : public Subject
{
public:
	void registerObserver(Observer* observer) override;

	void removeObserver(Observer* observer) override;

	void notifyObservers() override;

	void setMessage(std::string);
private:
	std::vector<Observer*> observers;
	std::string message;
};
#endif // !SIMULATION_DATA_H