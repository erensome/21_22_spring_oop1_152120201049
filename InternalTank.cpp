#include"InternalTank.h"

InternalTank::InternalTank(double capacity) : Tank(capacity)
{
	this->setValveStatus(1);
	this->setFuelQuantity(0);
}

void InternalTank::Update(string message_)
{
	cout << "Internal Tank " << getTankID() << ": " << message_ << endl;
}
