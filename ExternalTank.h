#ifndef EXTERNALTANK_H
#define EXTERNALTANK_H
#include "Tank.h"
using namespace std;
class ExternalTank : public Tank
{
public:
	ExternalTank(double capacity);
	void Update(string message_);
	//Objede modify yapilacagi icin const olmamali.
	ExternalTank operator+(ExternalTank& eTank);
};
#endif // !EXTERNALTANK_H