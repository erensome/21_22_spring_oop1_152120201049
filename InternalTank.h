#ifndef INTERNALTANK_H
#define INTERNALTANK_H
#include"Tank.h"
//The engine has its internal tank to store fuel. Internal tank capacity will be 55.0
class InternalTank : public Tank
{
public:
	InternalTank(double capacity);
	void Update(string message_);
};
#endif // !INTERNALTANK_H
