#include"Engine.h"

//Private ctor.
Engine::Engine()
{
	this->status = 0;
	this->heat = 20;
	this->health = 100;
	this->fullThrottle = 0;
	this->fuelPerSecond = 5.5;
}
void Engine::setStatus(bool status) { this->status = status; }
void Engine::setQuantityOfInternalTank(double fuel)
{
	iTank.setFuelQuantity(fuel);
}
void Engine::setFuelPerSecond(double value)
{
	this->fuelPerSecond = value;
}
void Engine::EngineHeatUp(int seconds)
{
	if (this->fullThrottle) // full throttle a��k.
	{
		if (this->heat < 90)
		{
			if (this->health > 0) this->health -= 1 * seconds;
		}
		this->heat += (seconds * 5);
	}//motor a��k de�ilken sorun ��kartabilir.!!!
	else // idle mod.
	{
		if (this->heat < 90)
		{
			this->heat += seconds * 1;
		}
	}
	if (this->heat > 130)
	{
		if (this->health > 0) this->health -= 1;
	}

}
void Engine::EngineCoolDown()
{
	if (this->heat > 20) //20den y�ksek
	{
		this->heat -= 1; // d���r
	}
}
void Engine::setHealth(int health)
{
	this->health = health;
}
void Engine::setHeat(int heat)
{
	this->heat = heat;
}
void Engine::setFullThrottle(bool status)
{
	this->fullThrottle = status;
}
bool Engine::getStatus() { return status; }
bool Engine::isFullThrottle()
{
	return this->fullThrottle;
}

double Engine::getFuelPerSecond()
{
	return this->fuelPerSecond;
}
double Engine::getQuantityOfInternalTank()
{
	return iTank.getFuelQuantity();
}
int Engine::getHealth()
{
	return this->health;
}
int Engine::getHeat()
{
	return this->heat;
}
Engine* Engine::getInstance()
{
	if (instance == NULL)
	{
		instance = new Engine;
	}
	return instance;
}
Engine::~Engine()
{
}
InternalTank* Engine::getITank()
{
	return &iTank;
}
void Engine::Update(string message_)
{
	cout << "Engine: " << message_ << endl;
}
