#ifndef _ENGINE_H
#define _ENGINE_H
#include"Observer.h"
#include"ExternalTank.h"
#include"InternalTank.h"
#include<iostream>
#include<vector>

//The engine has its internal tank to store fuel.
//Singleton Design Pattern uygulanacak. Uyguland�
using namespace std;
class Engine : public Observer
{
public:

	//Destructor
	~Engine();

	static Engine* getInstance();
	InternalTank* getITank();
	void Update(string) override;
	void setStatus(bool);
	void setQuantityOfInternalTank(double);
	void setFuelPerSecond(double);
	void EngineHeatUp(int);
	void EngineCoolDown();
	void setHealth(int);
	void setHeat(int);
	void setFullThrottle(bool);
	bool getStatus();
	bool isFullThrottle();
	double getFuelPerSecond();
	double getQuantityOfInternalTank();
	int getHealth();
	int getHeat(); // test i�in
	vector<Tank*>ConnectedTanks;
private:

	//private constructor for singleton.
	Engine();

	static Engine* instance; //Singleton instance

	//The engine has its internal tank to store fuel. Internal tank capacity will be 55.0
	//Cannot call ctor in class definition.
	InternalTank iTank = InternalTank(55.0);

	bool fullThrottle;
	int heat; // idle->90, stopped->20, full throttle-unlimited.
	int health;
	double fuelPerSecond;
	bool status; //status->working.
};
#endif // !_ENGINE_H