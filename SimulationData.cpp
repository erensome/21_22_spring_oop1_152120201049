#include "SimulationData.h"

void SimulationData::registerObserver(Observer* observer)
{
	observers.push_back(observer);
}

void SimulationData::removeObserver(Observer* observer)
{
	auto iterator = std::find(observers.begin(), observers.end(), observer);
	if (iterator != observers.end()) { // observer found
		observers.erase(iterator); // remove the observer
	}
}

void SimulationData::notifyObservers()
{
	for (Observer* observer : observers) { // notify all observers
		observer->Update(message);
	}
}

void SimulationData::setMessage(std::string message_)
{
	this->message = message_;
	notifyObservers();
}
