#pragma once
#include<string>

using namespace std;

//  Dependency Inversion Principle.
//! Soyut olarak tan�mlad���m�z observer aray�z�n�n d���k seviye s�n�flara ba��ml�l��� bulunmuyor.
//! Y�ksek seviye bir s�n�f�n alt seviye s�n�flara olan ba��ml�l���n� ortadan kald�rarak art�k
//! soyut katman �zerinden i�lemleri yapabiliyoruz.
class Observer
{
public:
	virtual void Update(string) = 0;
};