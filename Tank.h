#ifndef _TANK_H
#define _TANK_H
#include <iostream>
#include"Valve.h"
#include"Observer.h"
#include"Subject.h"
using namespace std;
class Tank : public Observer
{
public:
	//Constructor
	Tank(double _capacity);

	Valve* getValve();
	void incrementFlag();
	void setFuelQuantity(double);
	void setValveStatus(bool); //broken icin.
	void setStatus(bool);
	bool getValveStatus();
	bool IsBroken();
	double getCapacity();
	double getFuelQuantity();
	int getTankID();
private:
	Tank();

	bool broken;
	double fuel_quantity;
	//ID i�in flag. Her obje icin ayr� degil. Sinifin flagi.
	static int flag;
	int TankId;
	//cannot call ctor in class.
	Valve tankValve = Valve(flag);
	const double capacity;
};
// All fuel tanks� valves are closed as default. If a fuel tank�s valve is open, that tank can�t be 
//removed.
//There is no max tank count(Unlimited) (Tip: Use list instead of array.)
#endif // !_TANK_H