#pragma once
#include<list>
#include<vector>
#include<iterator>
#include<string>
#include"Observer.h"
using namespace std;

//  Open-Closed Principle.
//! Herhangi bir yeni Publisher(Subject) objesi olu�turmam�z gerekti�inde yapmam�z gereken Subject sinifi
//! �zerinde degisiklik degil, ki degisiklige kapali olmam�z gerek(Closed). Subject nesnemizden
//! yeni Publisheri t�retmek ve virtual metotlar� yeni Publisher icinde doldurmak. B�ylece
//! geni�lemeye a��k(open) oluyoruz. Ve hi�bir yerde de�i�iklik yapmam�z gerekmiyor.(Subject sinifinda)

//  Liskov Substitution Principle.
//! Subject sinifindan t�reyen alt siniflar, �st sinifin t�m �zelliklerini ve metotlar�n�
//! ayn� i�levi g�sterecek �ekilde kullanabilmektedir ve ayr�ca kendine ait yeni �zellikler bar�nd�rabilirler.
//! SimulationData sinifini inceleyebilirsiniz.

class Subject
{
public:
	virtual void registerObserver(Observer* observer) = 0;
	virtual void removeObserver(Observer* observer) = 0;
	virtual void notifyObservers() = 0;
};